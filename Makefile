.PHONY: init clean clean-dist dist

init: dist

clean: clean-dist

DIST_FILES=dev/docker/etc/.env
dist: $(DIST_FILES)
clean-dist:
	rm -f $(DIST_FILES)
$(DIST_FILES):
	cp $@.example $@