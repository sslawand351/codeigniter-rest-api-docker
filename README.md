# Codeigniter REST Api Using Docker

This repo contains codeigniter REST Api.

## Local Environment

We use docker to manage to our local stack to simulate a production environment as much is feasible. Effectively setting up and managing the stack requires a basic understanding of the following tools:   

- [Docker](https://docs.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/gettingstarted/)

**Quick References**

- Starting the Containers: `docker-compose start`
- Shell access to the web container: `docker-compose exec web bash` 
- View the site at: http://127.0.0.1/

### Setup

1. Make sure you have docker and docker-compose installed for your machine.

After cloning the repository, you'll need the following steps:

1. Initialize the repository and copy over any environment files with: `make init`
2. Start up containers: `docker-compose up`

### Rest Api

[View our REST Api doc for more information.](doc/rest-api.md)

### Socket.io

Socket.io is a javascript library which enable realtime notification, instant messaging,online gaming and all other real time events.It works on every platform browser or device.

1. Install express and socket.io
- `npm install express --save`
- `npm install socket.io --save`
