<?php 
	// header('Access-Control-Allow-Origin: *');
	/*$jsonfile = fopen("card_list.json", "r") or die("Unable to open file!");
	$json_string = fread($jsonfile, filesize("card_list.json"));
	fclose($jsonfile);

	echo $json_string;*/

$post = file_get_contents('php://input');
$post = json_decode($post, true);
// echo '<pre>'; print_r($post); exit;
$method = isset($post['method']) ? $post['method'] : '';
// echo $method;
switch ($method) {
	case 'get_all_card_list':
		$json_string = get_all_card_list($post);
		break;
	
	case 'add_card_list':
		$json_string = add_card_list($post);
		break;

	case 'delete_card_list':
		$json_string = delete_card_list($post);
		break;
	
	default:
		$json_string = get_all_card_list($post);
		break;
}

echo $json_string;	


function get_all_card_list($post)
{
	$jsonfile = fopen("card_list.json", "r") or die("Unable to open file!");
	$json_string = fread($jsonfile, filesize("card_list.json"));
	fclose($jsonfile);

	return $json_string;
}


function add_card_list($post)
{
	$card_list = $post['card_list'];
	$json_card_list = json_encode($card_list);

	$jsonfile = fopen("card_list.json", "r") or die("Unable to open file!");
	$json_string = fread($jsonfile, filesize("card_list.json"));
	fclose($jsonfile);
	$json_decode = json_decode($json_string, true);

	$jsonfile = fopen("card_list.json", "w") or die("Unable to open file!");

	array_push($json_decode['card_list'], $card_list);

	$json_string = fwrite($jsonfile, json_encode($json_decode, JSON_PRETTY_PRINT));
	fclose($jsonfile);

	return $json_card_list;
}

function delete_card_list($post)
{
	$card_list = $post['card_list'];
	$json_card_list = json_encode($card_list);

	$jsonfile = fopen("card_list.json", "r") or die("Unable to open file!");
	$json_string = fread($jsonfile, filesize("card_list.json"));
	fclose($jsonfile);
	$json_decode = json_decode($json_string, true);
/*echo '<pre>'; print_r($json_decode); 
json_encode($json_decode, JSON_PRETTY_PRINT);*/
	delete_card($card_list['id']);

	$key = array_search($card_list['id'], array_column($json_decode['card_list'], 'id'));
	if($key >= 0)
		unset($json_decode['card_list'][$key]);

	$json_decode['card_list'] = array_merge($json_decode['card_list']);
	$jsonfile = fopen("card_list.json", "w") or die("Unable to open file!");
	$json_string = fwrite($jsonfile, json_encode($json_decode, JSON_PRETTY_PRINT));
	fclose($jsonfile);

	return $json_card_list;
}

function delete_card($list_id)
{

	$jsonfile = fopen("cards.json", "r") or die("Unable to open file!");
	$json_string = fread($jsonfile, filesize("cards.json"));
	fclose($jsonfile);
	$json_decode = json_decode($json_string, true);

	$keys = array();
	foreach ($json_decode['cards'] as $k => $v) {
		if($v['listId'] == $list_id)
		{
			array_push($keys, $k);
		}
	}

	if(count($keys) > 0) {
		foreach ($keys as $k => $v) {
			unset($json_decode['cards'][$v]);
		}
	}
	$json_decode['cards'] = array_merge($json_decode['cards']);
// 	echo json_encode($json_decode, JSON_PRETTY_PRINT);
// print_r($json_decode); exit;

	$jsonfile = fopen("cards.json", "w") or die("Unable to open file!");

	$json_string = fwrite($jsonfile, json_encode($json_decode, JSON_PRETTY_PRINT));
	fclose($jsonfile);

	return true;
}