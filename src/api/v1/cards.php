<?php 
	// header('Access-Control-Allow-Origin: *');
	/*$jsonfile = fopen("cards.json", "r") or die("Unable to open file!");
	$json_string = fread($jsonfile, filesize("cards.json"));
	fclose($jsonfile);

	echo $json_string;*/


$post = file_get_contents('php://input');
$post = json_decode($post, true);
// echo '<pre>'; print_r($post); exit;
$method = isset($post['method']) ? $post['method'] : '';
// echo $method;
switch ($method) {
	case 'get_all_cards':
		$json_string = get_all_cards($post);
		break;
	
	case 'add_card':
		$json_string = add_card($post);
		break;

	case 'delete_card':
		$json_string = delete_card($post);
		break;

	case 'move_card':
		$json_string = move_card($post);
		break;
	
	default:
		$json_string = get_all_cards($post);
		break;
}

echo $json_string;	


function get_all_cards($post)
{
	$jsonfile = fopen("cards.json", "r") or die("Unable to open file!");
	$json_string = fread($jsonfile, filesize("cards.json"));
	fclose($jsonfile);

	return $json_string;
}


function delete_card($post)
{
	$card = $post['card'];
	$json_card = json_encode($card);

	$jsonfile = fopen("cards.json", "r") or die("Unable to open file!");
	$json_string = fread($jsonfile, filesize("cards.json"));
	fclose($jsonfile);
	$json_decode = json_decode($json_string, true);

	$key = false;
	foreach ($json_decode['cards'] as $k => $v) {
		if($v['id'] == $card['id'])
		{
			$key = $k;
		}
	}

	if($key !== false && $key >= 0) {
		unset($json_decode['cards'][$key]);
	}

	$json_decode['cards'] = array_merge($json_decode['cards']);
	$jsonfile = fopen("cards.json", "w") or die("Unable to open file!");
	$json_string = fwrite($jsonfile, json_encode($json_decode, JSON_PRETTY_PRINT));
	fclose($jsonfile);

	return $json_card;
}

function add_card($post)
{
	$card = $post['card'];
	$json_card = json_encode($card);

	$jsonfile = fopen("cards.json", "r") or die("Unable to open file!");
	$json_string = fread($jsonfile, filesize("cards.json"));
	fclose($jsonfile);
	$json_decode = json_decode($json_string, true);

	$jsonfile = fopen("cards.json", "w") or die("Unable to open file!");

	array_push($json_decode['cards'], $card);

	$json_string = fwrite($jsonfile, json_encode($json_decode, JSON_PRETTY_PRINT));
	fclose($jsonfile);

	return $json_card;
}

function move_card($post) {
	$card = $post['card'];
	$to_card_list_id = $post['to_card_list_id'];
	$json_card = json_encode($card);

	$jsonfile = fopen("cards.json", "r") or die("Unable to open file!");
	$json_string = fread($jsonfile, filesize("cards.json"));
	fclose($jsonfile);
	$json_decode = json_decode($json_string, true);

	$key = array_search($card['id'], array_column($json_decode['cards'], 'id'));
	if($key >= 0)
	{
		$json_decode['cards'][$key]['listId'] = $to_card_list_id;
	}

	$jsonfile = fopen("cards.json", "w") or die("Unable to open file!");
	$json_string = fwrite($jsonfile, json_encode($json_decode, JSON_PRETTY_PRINT));
	fclose($jsonfile);

	return $json_card;
}