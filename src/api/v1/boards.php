<?php
// echo '<pre>'; print_r($post); exit;
$post = file_get_contents('php://input');
$post = json_decode($post, true);
// echo '<pre>'; print_r($post); exit;
$method = isset($post['method']) ? $post['method'] : '';
// echo $method;
switch ($method) {
	case 'get_all_boards':
		$json_string = get_all_boards($post);
		break;
	
	case 'add_board':
		$json_string = add_board($post);
		break;
	
	default:
		$json_string = get_all_boards($post);
		break;
}

echo $json_string;	


function get_all_boards($post)
{
	$jsonfile = fopen("boards.json", "r") or die("Unable to open file!");
	$json_string = fread($jsonfile, filesize("boards.json"));
	fclose($jsonfile);

	return $json_string;
}


function add_board($post)
{
	$board = $post['board'];
	$json_board = json_encode($board);

	$jsonfile = fopen("boards.json", "r") or die("Unable to open file!");
	$json_string = fread($jsonfile, filesize("boards.json"));
	fclose($jsonfile);
	$json_decode = json_decode($json_string, true);

	$jsonfile = fopen("boards.json", "w") or die("Unable to open file!");

	array_push($json_decode['boards'], $board);

	$json_string = fwrite($jsonfile, json_encode($json_decode, JSON_PRETTY_PRINT));
	fclose($jsonfile);

	return $json_board;
}