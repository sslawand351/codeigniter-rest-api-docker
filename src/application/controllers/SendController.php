<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SendController extends CI_Controller
{
	private $MESSAGE_JSON = 'resources/db/message.json';
    public function index()
    {
    	// print_r(dirname(__DIR__));
    	// print_r(count($this->get()['messages']));
        $data = array();
        // $allmsgs = $this->db->select('*')->from('tbl_msg')->get()->result_array();
        $allmsgs = $this->get()['messages'] ?? [];
// print_r($this->count());
        $data['allMsgs'] = $allmsgs;
        $data['msgcount'] = count($allmsgs);
        $this->load->view('message', $data);
    }

    public function send()
    {
        $arr['msg'] = $this->input->post('message');
        $arr['date'] = date('Y-m-d');
        $arr['status'] = 1;
        $detail = $this->insert($arr);
        // $this->db->insert('tbl_msg',$arr);
        // $detail = $this->db->select('*')->from('tbl_msg')->where('id',$this->db->insert_id())->get()->row();
        // $msgCount = $this->db->select('*')->from('tbl_msg')->get()->num_rows();
        // print_r($this->count());
        $msgCount = $this->count();
        $arr['message'] = $detail['msg'];
        $arr['date'] = date('m-d-Y', strtotime($detail['date']));
        $arr['msgcount'] = $msgCount;
        $arr['success'] = true;
        echo json_encode($arr);
    }

    private function insert($arr) {
    	$jsonfile = fopen($this->MESSAGE_JSON, "r") or die("Unable to open file!");
		$json_string = fread($jsonfile, filesize($this->MESSAGE_JSON));
		fclose($jsonfile);
		$json_decode = json_decode($json_string, true);

		$jsonfile = fopen($this->MESSAGE_JSON, "w") or die("Unable to open file!");

		$json_decode['messages'] = $json_decode['messages'] ?? [];
		array_push($json_decode['messages'], $arr);

		$json_string = fwrite($jsonfile, json_encode($json_decode, JSON_PRETTY_PRINT));
		fclose($jsonfile);
		clearstatcache();
		return $arr;
    }

    private function get() {
    	$jsonfile = fopen($this->MESSAGE_JSON, "r") or die("Unable to open file!");
		$json_string = fread($jsonfile, filesize($this->MESSAGE_JSON));
		fclose($jsonfile);

		return json_decode($json_string, true);
    }

    private function count() {
    	$messages = $this->get()['messages'] ?? [];
    	return count($messages);
    }
}
