#!/usr/bin/env bash

DOCUMENT_ROOT=/var/www/html

disable_dev_php_mods() {
  echo "disable newrelic and xdebug"
  phpdismod newrelic
  phpdismod xdebug
}

tail_php_error_log() {
  echo "touch php error log"
  touch /var/log/apache2/php_error.log && chmod 777 /var/log/apache2/php_error.log

  echo "tail php_error.log"
  tail -f /var/log/apache2/php_error.log &
}

link_local_timezone() {
  echo "link New_York timezone"
  unlink /etc/localtime && ln -s /usr/share/zoneinfo/America/New_York /etc/localtime
}

# create a pipe for app logs to write into and this process to read from to stdout
create_app_log_pipe() {
  echo "Creating app-log-pipe"
  mkfifo ${DOCUMENT_ROOT}/src/var/log/app-log-pipe
  chmod 666 ${DOCUMENT_ROOT}/src/var/log/app-log-pipe
  tail -f ${DOCUMENT_ROOT}/src/var/log/app-log-pipe &
}

setup_fpm_run_dir() {
  if [[ ! -d /run/php ]]; then
    echo "Initializing /run/php"
    mkdir -p /run/php
  fi
}

setup_apache_environment() {
  # Allow PORT env to apache listen port
  if [[ -n $PORT ]]; then
    echo "Listening to Port: ${PORT}"
    echo "Listen $PORT" > /etc/apache2/ports.conf
  fi
}

get_heroku_proc_type() {
  if [[ -z $DYNO ]]; then
    echo "No DYNO environment variable is set, cannot determine the heroku proc type"; exit 1;
  fi

  echo $DYNO | sed -e s/\\.[0-9]*//g
}

determine_supervisord_conf_file() {
  if [[ $PLATFORM == "heroku" ]]; then
    proc=$(get_heroku_proc_type)
    echo "supervisord-heroku-${proc}.conf"
  else
    echo "supervisord-local.conf"
  fi
}

main() {
  link_local_timezone
  setup_fpm_run_dir
  setup_apache_environment

  echo "Printing env"
  printenv | sort

  tail_php_error_log
}

main
